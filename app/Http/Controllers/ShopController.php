<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\Shops;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public  function  store(Request $request)
    {
//        $save_data['id'] = $request->shop_id;
        $save_data['name'] = $request->name;
        $save_data['phone'] = $request->phone;
        $save_data['email'] = $request->email;
        $save_data['website'] = $request->website;
        $save_data['created_by'] = $request->user_id;
        $save_data['updated_by'] = $request->user_id;
        $save_data['lat'] = $request->lat;
        $save_data['lng'] = $request->Long;
        $save_data['description'] = $request->description;
        $save_data['image'] = $request->image;
//        return $save_data;

//
//        $save_data['id'] = $request->shop_id;
//        $save_data['name'] = 'name';
//        $save_data['phone'] ='0234342';
//        $save_data['email'] = 'email';
//        $save_data['website'] = 'website';
//        $save_data['created_by'] = 1;
//        $save_data['updated_by'] = 1;
//        $save_data['lat'] = '43.3485345353953';
//        $save_data['lng'] = '344.45834538574';
//        $save_data['description'] = 'ewiuewufhed';


        $obj = new Shops();

        if($save_id = $obj->saveAll(0,$save_data)){

//            dd(Shops::find($save_id));
            return  ['data' => $save_id];

        }
        else{

            return "error";
        }


    }

    public  function  update(Request $request)
    {

        $save_data['id'] = $request->shop_id;
        $save_data['name'] = $request->name;
        $save_data['phone'] = $request->phone;
        $save_data['email'] = $request->email;
        $save_data['website'] = $request->website;
        $save_data['updated_by'] = $request->user_id;
        $save_data['image'] = $request->image;
        $save_data['lat'] = $request->lat;
        $save_data['lng'] = $request->Long;
        $save_data['description'] = $request->description;


//        dd($save_data);
        $obj = new Shops();

        if($save_id = $obj->saveAll($save_data['id'],$save_data)){

            return  Shops::find($save_id);

        }
        else{

            return "error";
        }


    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShopListByOwnerID(Request $request)
    {
        $owner_id = $request->user_id;
        $obj = new Shops();
        return ['data' => $obj->getShopListByOwnerId($owner_id)];
    }

    public function getOtherShopList(Request $request)
    {
        $user_id = $request->user_id;
        $obj = new Shops();
        return ['data' => $obj->getOtherShopList($user_id)];
    }


    public function getShopByID(Request $request)
    {
        $shop_id = $request->shop_id;
        $shop_data = Shops::getShopById($shop_id);
        $shop_data = $shop_data[0];
        return ['data' => $shop_data];
    }

    public function getContactByShopID(Request $request)
    {
        $shop_id = $request->shop_id;
        return ['data' => Shops::getContactByShopId($shop_id)];
    }

    public function delete(Request $request)
    {
        $shop_id = $request->shop_id;
        $save_data['created_by'] = $request->user_id;
        $save_data['is_deleted'] = true;


        $obj = new Shops();
        if($save_id = $obj->saveAll($shop_id,$save_data)){
            return ['data' => $save_id];
        }
        else{
            return "error";
        }
    }




}
