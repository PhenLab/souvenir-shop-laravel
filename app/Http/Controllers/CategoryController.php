<?php

namespace App\Http\Controllers;

use App\Categories;
use App\District;
use App\Province;
use App\Shops;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public  function  store(Request $request)
    {
        $save_data['id'] = $request->shop_id;
        $save_data['name'] = $request->name;
        $save_data['phone'] = $request->phone;
        $save_data['email'] = $request->email;
        $save_data['website'] = $request->website;
        $save_data['created_by'] = $request->user_id;
        $save_data['updated_by'] = $request->user_id;

        $save_data['image'] = $request->image;

        $obj = new Shops();

        if($save_id = $obj->saveAll($save_data['id'],$save_data)){

//            dd(Shops::find($save_id));
            return  Shops::find($save_id);

        }
        else{

            return "error";
        }


    }

    public  function  update(Request $request)
    {

        $save_data['id'] = $request->shop_id;
        $save_data['name'] = $request->name;
        $save_data['phone'] = $request->phone;
        $save_data['email'] = $request->email;
        $save_data['website'] = $request->website;
        $save_data['updated_by'] = $request->user_id;
        $save_data['image'] = $request->image;


//        dd($save_data);
        $obj = new Shops();

        if($save_id = $obj->saveAll($save_data['id'],$save_data)){

            return  Shops::find($save_id);

        }
        else{

            return "error";
        }


    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategoryList(Request $request)
    {

        return ['data' => Categories::getCategoryList()];
    }

    public function getOtherShopList(Request $request)
    {
        $user_id = $request->user_id;
        $obj = new Shops();
        return ['data' => $obj->getOtherShopList($user_id)];
    }


    public function getShopByID(Request $request)
    {
        $shop_id = $request->shop_id;
        return ['data' => Shops::getShopById($shop_id)];
    }
}
