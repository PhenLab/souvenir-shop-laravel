<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\Shops;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public  function  updateImage(Request $request)
    {

        $data['id'] = $request->id;
        $data['image'] = $request->image;
        $obj = new User();

        if($save_id = $obj->saveAll($data['id'],$data)){

            return ['data' => User::getUserById($save_id)];

        }
        else{

            return "error";
        }


    }

    public  function  update(Request $request)
    {

        $data['id'] = $request->id;
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['user_name'] = $request->user_name;
        $data['phone'] = $request->phone;
        $data['email'] = $request->email;

        $obj = new User();

        if($save_id = $obj->saveAll($data['id'],$data)){

            return ['data' => User::getUserById($save_id)];

        }
        else{

            return "error";
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function getUserByID(Request $request)
    {
        $user_id = $request->user_id;
        return ['data' => User::getUserById($user_id)];
    }
}
