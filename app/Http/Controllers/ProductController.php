<?php

namespace App\Http\Controllers;

use App\District;
use App\Products;
use App\Province;
use App\Shops;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use function Psy\sh;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public  function  store(Request $request)
    {
        $save_data['name'] = $request->name;
        $save_data['category_id'] = $request->category_id;
        $save_data['quantity'] = $request->quantity;
        $save_data['price'] = $request->price;
        $save_data['created_by'] = $request->user_id;
        $save_data['updated_by'] = $request->user_id;
        $save_data['shop_id'] = $request->shop_id;
        $save_data['image'] = $request->image;
        $save_data['description'] = $request->description;

//
//        $save_data['name'] = "kh";
//        $save_data['category_id'] = '1';
//        $save_data['quantity'] = '867';
//        $save_data['price'] = 867;
//        $save_data['created_by'] = 1;
//        $save_data['updated_by'] = 1;
//        $save_data['shop_id'] = 1;
//
//        $save_data['image'] = "";
//

        $obj = new Products();

        if($save_id = $obj->saveAll(0,$save_data)){

            return  Products::find($save_id);

        }
        else{

            return "error";
        }


    }

    public  function  update(Request $request)
    {

        $save_data['name'] = $request->name;
        $save_data['category_id'] = $request->category_id;
        $save_data['quantity'] = $request->quantity;
        $save_data['price'] = $request->price;
        $save_data['updated_by'] = $request->user_id;
        $save_data['image'] = $request->image;
        $save_data['description'] = $request->description;
        $save_data['id'] = $request->id;


        $obj = new Products();

        if($save_id = $obj->saveAll($save_data['id'],$save_data)){

            return  Products::find($save_id);

        }
        else{

            return "error";
        }


    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductListByShopID(Request $request)
    {
        $shop_id = $request->shop_id;
        return ['data' => Products::getProductListByShopId($shop_id)];
    }

    public function getProductList(Request $request)
    {
        return ['data' => Products::getProductList()];
    }


    public function getProductListByCategoryID(Request $request)
    {
        $category_id = $request->category_id;
        $current_id = $request->id;

        return ['data' => Products::getProductListByCategoryID($category_id,$current_id)];
    }

    public function getOtherProductList(Request $request)
    {
        $user_id = $request->user_id;
        return ['data' => Products::getOtherProducts($user_id)];
    }

    public function getProductById(Request $request)
    {
        $product_id = $request->id;

        return ['data' => Products::getProductById($product_id)];
    }


    public function getProductNumberByShopID(Request $request)
    {
        $shop_id = $request->shop_id;
        return ['data' => Products::getProductCountByShopId($shop_id)];
    }

    public function delete(Request $request)
    {
        $product_id = $request->id;
        $save_data['created_by'] = $request->user_id;
        $save_data['is_deleted'] = true;


        $obj = new Products();
        if($save_id = $obj->saveAll($product_id,$save_data)){
            return ['data' => $save_id];
        }
        else{
            return "error";
        }
    }

}
