<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/register',
        '/login',
        'shop/add',
        'get-category-list',
        'shop/update',
        'get-shop-by-owner-id',
        '/get-shop-by-id',
        '/get-user-by-id',
        '/update-user',
        '/update-user-image',
        '/get-other-shop-by-owner-id',
        '/get-shop-by-id',
        'shop/delete',
        '/category/list',
        '/product/add',
        '/get-product-list-by-shop-id',
        '/get-product-list',
        '/get-product-by-id',
        '/product/count-by-shop-id',
        '/product/delete',
        '/product/update',
        'shop/get-contacts',
        '/get-product-list-by-category-id',
        '/get-other-product-list',
        '/favorite/add',
        '/favorite/delete',


    ];
}
