<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Shops extends Authenticatable
{
    use Notifiable;
    protected  $table = 'shops';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

//product_numbers
    public function saveAll($id = 0, $data){

        try{

            $data['updated_at'] = date('Y-m-d H:i:s');

            if($id){
                $this->id = $id;
                DB::table($this->table)
                    ->where('id','=',$id)
                    ->update($data);
            }else{

                $data['created_at'] = date('Y-m-d H:i:s');


                $obj = new Shops();
                foreach($data as $key => $value){
                    $obj->$key = @$value;
                }
                $obj->save();
                $this->id = $obj->id;
            }

            return $this->id;
        }catch(Exception $e){
            return $e->getMessage();
        }

    }

    public function getShopListByOwnerId($id = 0){

//        $query = DB::table($this->table)
//            ->select(array(
//                'shops.*',
//                DB::raw('COUNT(products.shop_id) as product_numbers')
//            ));
//
//        $query->leftJoin('products','products.shop_id','=','shops.id')
////        $query->where('shops.created_by', $id)
//            ->where('shops.status', true)
//            ->where('shops.is_deleted', false)
//            ->where('products.is_deleted', false)
//            ->where('products.status', true)
//            ->groupBy('shops.id')
//            ->orderBy('shops.updated_at', 'DES');
//        return $query->get();

        $query = 'SELECT s.*, COUNT(p.shop_id) as product_numbers
                    FROM shops as s 
                    LEFT JOIN products as p
                        ON s.id = p.shop_id AND p.status = 1 AND p.is_deleted = 0
                      
                    ';

        $where = " WHERE s.status = 1 ";
        $where .= " AND s.created_by = ".$id;
        $where .= " AND s.is_deleted = 0 GROUP BY s.id ORDER BY s.updated_at DESC  ";

        $query .= $where;
        $result = DB::select($query);

        return $result;

    }

    public function getOtherShopList($id = 0){

//        $query = DB::table($this->table)
//            ->select(array(
//                'shops.*',
//            ))
//            ->where('created_by','!=', $id)
//            ->where('status', true)
//            ->where('is_deleted', false)
//            ->orderBy('updated_at', 'DES');
//        return $query->get();

        $query = 'SELECT s.*, COUNT(p.shop_id) as product_numbers
                    FROM shops as s 
                    LEFT JOIN products as p
                        ON s.id = p.shop_id AND p.status = 1 AND p.is_deleted = 0
                      
                    ';

        $where = " WHERE s.status = 1 ";
        $where .= " AND s.created_by <> ".$id;
        $where .= " AND s.is_deleted = 0 GROUP BY s.id ORDER BY s.updated_at DESC  ";

        $query .= $where;
        $result = DB::select($query);

        return $result;

    }

    public static function getShopById($id = 0){

//        $query = DB::table('shops')
//            ->select(array(
//                'shops.*',
//            ))
//            ->where('id', $id)
//            ->where('status', true)
//            ->where('is_deleted', false);
//        return $query->first();

        $query = 'SELECT s.*, COUNT(f.item_id) as is_favorite
                    FROM shops as s 
                    LEFT JOIN favorites as f
                        ON f.item_id = s.id AND f.is_deleted = 0 AND f.type = 1 AND f.item_id = '.$id;

        $where = " WHERE s.status = 1 ";
        $where .= " AND s.id = ".$id;
        $where .= " AND s.is_deleted = 0 GROUP BY s.id";

        $query .= $where;
        $result = DB::select($query);

        return $result;

    }

    public static function getContactByShopId($id = 0){

        $query = DB::table('shops')
            ->select(array(
                'shops.phone',
                'shops.email',
                'shops.website',

            ))
            ->where('id', $id)
            ->where('status', true)
            ->where('is_deleted', false);
        return $query->first();

    }

}
