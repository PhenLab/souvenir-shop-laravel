<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Products extends Authenticatable
{
    use Notifiable;
    protected  $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function saveAll($id = 0, $data){

//        $userInfo = Auth::user();

        try{

            $data['updated_at'] = date('Y-m-d H:i:s');
//            $data['updated_by'] = 1;

            if($id){
                $this->id = $id;
                DB::table($this->table)
                    ->where('id','=',$id)
                    ->update($data);
            }else{

                $data['created_at'] = date('Y-m-d H:i:s');
//                $data['created_by'] = 1;

                $obj = new Products();
                foreach($data as $key => $value){
                    $obj->$key = @$value;
                }
                $obj->save();
                $this->id = $obj->id;
            }

            return $this->id;
        }catch(Exception $e){
            return $e->getMessage();
        }

    }


    public static function getProductListByShopId($id = 0){

        $query = DB::table('products')
            ->select(array(
                'products.*',
                'shops.name as shop_name',
                'categories.name as category_name'
            ));

        $query->leftJoin('shops', 'shops.id', '=', 'products.shop_id');
        $query->leftJoin('categories', 'categories.id', '=', 'products.category_id');

        $query->where('products.shop_id', $id);
        $query->where('products.is_deleted', false);

        $query->where('products.status', true);
        $query->orderBy('products.updated_at', 'DES');
        return $query->get();

    }

    public static function getProductList(){

        $query = DB::table('products')
            ->select(array(
                'products.*',
                'shops.name as shop_name',
                'categories.name as category_name'
            ));

        $query->leftJoin('shops', 'shops.id', '=', 'products.shop_id');
        $query->leftJoin('categories', 'categories.id', '=', 'products.category_id');

        $query->where('products.status', true);
        $query->orderBy('products.updated_at', 'DES');
        return $query->get();

    }

    public function getOtherShopList($id = 0){

        $query = DB::table($this->table)
            ->select(array(
                'shops.*',
            ))
            ->where('created_by','!=', $id)
            ->where('status', true)
            ->where('is_deleted', false)
            ->orderBy('updated_at', 'ASC');
        return $query->get();

    }

    public static function getProductListByCategoryID($id = 0,$current_id){

        $query = DB::table('products')
            ->select(array(
                'products.*',
                'shops.name as shop_name',
                'categories.name as category_name'
            ));

        $query->leftJoin('shops', 'shops.id', '=', 'products.shop_id');
        $query->leftJoin('categories', 'categories.id', '=', 'products.category_id');

        $query->where('products.category_id', $id);
        $query->where('products.id','!=', $current_id);
        $query->where('products.is_deleted', false);

        $query->where('products.status', true);
        $query->orderBy('products.updated_at', 'DES');
        return $query->get();

    }


    public static function getOtherProducts($user_id = 0){

        $query = DB::table('products')
            ->select(array(
                'products.*',
                'shops.name as shop_name',
                'categories.name as category_name'
            ));

        $query->leftJoin('shops', 'shops.id', '=', 'products.shop_id');

        $query->leftJoin('categories', 'categories.id', '=', 'products.category_id');

        $query->where('products.created_by','!=', $user_id);
        $query->where('products.status', true);
        $query->where('products.is_deleted', false);

        $query->orderBy('products.updated_at', 'DES');
        return $query->get();

    }

    public static function getProductById($id = 0){

//        $query = DB::table('products')
//            ->select(array(
//                'products.*',
//                'shops.name as shop_name',
//                'shops.image as shop_image',
//                'shops.lat as lat',
//                'shops.lng as lng',
//
//                'categories.name as category_name'
//            ));
//
//        $query->leftJoin('shops', 'shops.id', '=', 'products.shop_id');
////        $query->leftJoin('favorites', 'favorites.item_id', '=', 'products.id');
//
//        $query->leftJoin('categories', 'categories.id', '=', 'products.category_id');
//
//        $query->where('products.id','=', $id);
//        $query->where('products.status', true);
//        $query->orderBy('products.updated_at', 'DES');
//        return $query->get();


        $query = 'SELECT p.*, COUNT(f.item_id) as is_favorite,
                  s.name as shop_name, s.image as shop_image,
                  s.lat as lat,s.lng as lng,
                  c.name as category_name
                    FROM products as p
                    LEFT JOIN shops as s
                          ON s.id = p.shop_id
                    LEFT JOIN categories as c
                          ON c.id = p.category_id

                    LEFT JOIN favorites as f
                        ON f.item_id = p.id AND f.is_deleted = 0 AND f.type = 2 AND f.item_id = '.$id;

        $where = " WHERE p.status = 1 ";
        $where .= " AND p.id = ".$id;
        $where .= " GROUP BY p.id";

        $query .= $where;
        $result = DB::select($query);

        return $result;

    }


    public static function getProductCountByShopId($id = 0){

        $query = DB::table('products');

        $query->where('products.shop_id', $id);
        $query->where('products.is_deleted', false);

        $query->where('products.status', true);
        return $query->get()->count();

    }

}
