<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','user_name','phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function saveAll($id = 0, $data){

//        $userInfo = Auth::user();

        try{

            $data['updated_at'] = date('Y-m-d H:i:s');
//            $data['updated_by'] = 1;

            if($id){
                $this->id = $id;
                DB::table($this->table)
                    ->where('id','=',$id)
                    ->update($data);
            }else{

                $data['created_at'] = date('Y-m-d H:i:s');
//                $data['created_by'] = 1;

                $obj = new Shops();
                foreach($data as $key => $value){
                    $obj->$key = @$value;
                }
                $obj->save();
                $this->id = $obj->id;
            }

            return $this->id;
        }catch(Exception $e){
            return $e->getMessage();
        }

    }


    public static function getUserById($id = 0){

        $query = DB::table('users')
            ->select(array(
                'users.*',
            ))
            ->where('id', $id)
            ->where('status', true)
            ->where('is_deleted', false);
        return $query->first();

    }

}
