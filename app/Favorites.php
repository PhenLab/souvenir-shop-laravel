<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Favorites extends Authenticatable
{
    use Notifiable;
    protected  $table = 'favorites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function saveAll($id = 0, $data){

        try{

            $data['updated_at'] = date('Y-m-d H:i:s');

            if($id){
                $this->id = $id;
                DB::table($this->table)
                    ->where('id','=',$id)
                    ->update($data);
            }else{

                $data['created_at'] = date('Y-m-d H:i:s');


                $obj = new Favorites();
                foreach($data as $key => $value){
                    $obj->$key = @$value;
                }
                $obj->save();
                $this->id = $obj->id;
            }

            return $this->id;
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    public function getShopListByOwnerId($id = 0){

        $query = DB::table($this->table)
            ->select(array(
                'shops.*',
            ))
            ->where('created_by', $id)
            ->where('status', true)
            ->where('is_deleted', false)
            ->orderBy('updated_at', 'DES');
        return $query->get();

    }

    public function getOtherShopList($id = 0){

        $query = DB::table($this->table)
            ->select(array(
                'shops.*',
            ))
            ->where('created_by','!=', $id)
            ->where('status', true)
            ->where('is_deleted', false)
            ->orderBy('updated_at', 'DES');
        return $query->get();

    }

    public static function getShopById($id = 0){

        $query = DB::table('shops')
            ->select(array(
                'shops.*',
            ))
            ->where('id', $id)
            ->where('status', true)
            ->where('is_deleted', false);
        return $query->first();

    }

    public static function getContactByShopId($id = 0){

        $query = DB::table('shops')
            ->select(array(
                'shops.phone',
                'shops.email',
                'shops.website',

            ))
            ->where('id', $id)
            ->where('status', true)
            ->where('is_deleted', false);
        return $query->first();

    }

}
