<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('shop/add', 'ShopController@store');
Route::get('shop/add', 'ShopController@store');
Route::post('shop/update', 'ShopController@update');

Route::post('shop/delete', 'ShopController@delete');
Route::get('shop/delete', 'ShopController@delete');


Route::post('shop/get-contacts', 'ShopController@getContactByShopID');
Route::get('shop/get-contacts', 'ShopController@getContactByShopID');


Route::post('/get-shop-by-owner-id', 'ShopController@getShopListByOwnerID');
Route::get('/get-shop-by-owner-id', 'ShopController@getShopListByOwnerID');

Route::post('/get-shop-by-id', 'ShopController@getShopByID');
Route::get('/get-shop-by-id', 'ShopController@getShopByID');

Route::post('/get-other-shop-by-owner-id', 'ShopController@getOtherShopList');
Route::get('/get-other-shop-by-owner-id', 'ShopController@getOtherShopList');


Route::post('/get-user-by-id', 'UserController@getUserByID');
Route::get('/get-user-by-id', 'UserController@getUserByID');

Route::get('/update-user', 'UserController@update');
Route::post('/update-user-image', 'UserController@updateImage');
Route::post('/update-user', 'UserController@update');

Route::post('/product/add', 'ProductController@store');
Route::get('/product/add', 'ProductController@store');
Route::get('/get-product-list-by-shop-id', 'ProductController@getProductListByShopID');
Route::post('/get-product-list-by-shop-id', 'ProductController@getProductListByShopID');
Route::get('/get-product-list', 'ProductController@getProductList');
Route::post('/get-product-list', 'ProductController@getProductList');
Route::post('/get-product-list-by-category-id', 'ProductController@getProductListByCategoryID');
Route::get('/get-product-list-by-category-id', 'ProductController@getProductListByCategoryID');

Route::post('/get-other-product-list', 'ProductController@getOtherProductList');
Route::get('/get-other-product-list', 'ProductController@getOtherProductList');
Route::post('/get-product-by-id', 'ProductController@getProductById');
Route::get('/get-product-by-id', 'ProductController@getProductById');


Route::post('/product/update', 'ProductController@update');
Route::get('/product/update', 'ProductController@update');
Route::post('/product/delete', 'ProductController@delete');
Route::post('/product/count-by-shop-id', 'ProductController@getProductNumberByShopID');
Route::get('/product/count-by-shop-id', 'ProductController@getProductNumberByShopID');


Route::post('/category/list', 'CategoryController@getCategoryList');
Route::get('/category/list', 'CategoryController@getCategoryList');

Route::post('/favorite/add', 'FavoriteController@store');
Route::get('/favorite/add', 'FavoriteController@store');
Route::post('/favorite/delete', 'FavoriteController@delete');
Route::get('/favorite/delete', 'FavoriteController@delete');
