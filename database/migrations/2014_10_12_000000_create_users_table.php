<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('shop_id')->default(0);
            $table->integer('category_id')->default(0);
            $table->string('name');
            $table->double('price');
            $table->string('price_measure');
            $table->integer('quantity')->default(0);
            $table->string('description')->nullable();

            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);

            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);


            $table->timestamps();
        });

//        Schema::create('users', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('first_name');
//
//            $table->string('last_name');
//            $table->string('user_name');
//            $table->integer('phone')->default(0)->unique();
//
//            $table->boolean('status')->default(true);
//            $table->boolean('is_deleted')->default(false);
//
//
//            $table->string('email')->unique();
//            $table->string('password');
//            $table->rememberToken();
//
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
