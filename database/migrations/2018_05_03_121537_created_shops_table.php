<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('phone')->default(0)->unique();

            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->integer('lat')->default(0);
            $table->integer('long')->default(0);

            $table->string('email')->unique()->nullable();
            $table->string('website')->unique()->nullable();
            $table->string('description')->nullable();

            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
